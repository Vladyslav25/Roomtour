// Fill out your copyright notice in the Description page of Project Settings.


#include "BPFL.h"
#include "GameFramework/PlayerInput.h"

void UBPFL::FlushInputs(const APlayerController* PlayerController)
{
	PlayerController->PlayerInput->FlushPressedKeys();
}
