// Fill out your copyright notice in the Description page of Project Settings.


#include "SoundWithSub.h"

void USoundWithSub::SetDurationAt(int _index, float _duration)
{
	if(_index <= Subtitels.Num())
		Subtitels[_index].Duration = _duration;
}

void USoundWithSub::Validate()
{
	if(Sound != nullptr)
		for(size_t i = 0; i < Subtitels.Num(); i++)
		{
			if(Subtitels[i].Text[0] == '/')
				SetDurationAt(i);
		}
}
