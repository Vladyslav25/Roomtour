// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include <Runtime/MediaAssets/Public/FileMediaSource.h>
#include "TriggerInformation.generated.h"

USTRUCT(BlueprintType)
struct FImageText
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UTexture2D* Picture;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		FString ImageText;
};

USTRUCT(BlueprintType)
struct FVideo
{
	GENERATED_USTRUCT_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UTexture2D* Thumbnail;
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		UFileMediaSource* Source;
};

/**
 *
 */
UCLASS(BlueprintType)
class VIRTUELLSAE_API UTriggerInformation : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<FImageText> Images;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<FImageText> Logos;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<FVideo> EndProduktionen;

	UFUNCTION(BlueprintCallable)
		TArray<UTexture2D*> GetAllImages();

	UFUNCTION(BlueprintCallable)
		TArray<UTexture2D*> GetAllLogos();

	UFUNCTION(BlueprintCallable)
		TArray<UTexture2D*> GetAllThumbnails();
};