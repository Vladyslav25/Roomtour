// Fill out your copyright notice in the Description page of Project Settings.


#include "TriggerInformation.h"

TArray<UTexture2D*> UTriggerInformation::GetAllImages()
{
    TArray<UTexture2D*> output;

	for(size_t i = 0; i < Images.Num(); i++)
	{
		output.Add(Images[i].Picture);
	}

    return output;
}

TArray<UTexture2D*> UTriggerInformation::GetAllLogos()
{
	TArray<UTexture2D*> output;

	for(size_t i = 0; i < Logos.Num(); i++)
	{
		output.Add(Logos[i].Picture);
	}

	return output;
}

TArray<UTexture2D*> UTriggerInformation::GetAllThumbnails()
{
	TArray<UTexture2D*> output;

	for(size_t i = 0; i < EndProduktionen.Num(); i++)
	{
		output.Add(EndProduktionen[i].Thumbnail);
	}

	return output;
}
