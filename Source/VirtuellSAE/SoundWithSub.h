// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/DataAsset.h"
#include "Sound/SoundWave.h"
#include "SoundWithSub.generated.h"

UENUM(BlueprintType)
enum class ESpeaker : uint8
{
	TUENNES	UMETA(DisplayName = "Tuennes"),
	SCHAEL	UMETA(DisplayName = "Schael")
};

USTRUCT(BlueprintType)
struct FTextTime
{
	GENERATED_USTRUCT_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		FString Text;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		float Duration;
};

/**
 *
 */
UCLASS(BlueprintType)
class VIRTUELLSAE_API USoundWithSub : public UDataAsset
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		ESpeaker Speaker;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		USoundWave* Sound;

	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TArray<FTextTime> Subtitels;

	UFUNCTION(BlueprintCallable)
		void SetDurationAt(int _index, float _duration = 0);

	UFUNCTION(CallInEditor, Category="Validate", meta = (DisplayName="Validate Input"))
		void Validate();
};
