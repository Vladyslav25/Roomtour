# RoomTour

**Achtung dieses Projekt ist eine Prototyp Version!**

Was habe ich geamcht:
- Untertitel System
- UI + UI Animationen
- C++ DataAssets
- Blueprints
- Outline Shader

In diesem Projekt wollen wir einen virtuellen Rundgang unseres Campuses machen.
Und dazu dem Nutzer auch viel Info einzelner Räume und Ort geben.

Als Ziel für diese Projekt stelle ich mir, Tools für den Level Desinger zu erstellen, dass er so flexibel wie möglich die Triggerboxen, Untertitel und Biler einbauen kann. 
Das habe erreicht, indem ich DataAssets für Untertiel, wo eine Audiodatei an Texte gebunden ist, diese wiederum sind an eine Dauer gebunden wie lange diese als Untertitel erscheinen sollen. Zusätzlich wählt man einen Sprecher aus, welcher auftaucht. 
Später sollte der Sprecher wechseln können, bzw. der andere auftauchen. Dafür wurden Befehle eingebaut, welche man als Text einsetzten kann, um so auf die Sprecher-Anzeige zu beeinflussen zu können.
Für die Bilder und Videos erstellte ich auch DataAssets wo man ganz bequem die einzelnen Bilder für die Kategorie auswählen kann.
Auch erstellte ich einen Videoplayer, welcher auch die Endproduktionen einiger unserer Studenten zeigen kann. 

Der Nutzer kann an einigen Stellen auch mit Objekten Interagieren, damit die Information dazu erscheint. Zusätzlich zur Outline des Objektes, soll auch ein Pfeil über dem Objekt schweben um ihn darauf aufmerksam zu machen.
